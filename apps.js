
// Task 14 #1 //
function multiply( val1 , val2 ) {
    return val1 * val2; 
}

console.log(multiply(9 , 3));

// Task 14 #2 //
function sum3( val1 , val2, val3 ) {
    return val1 + val2 + val3; 
}

console.log(sum3( 10, 4, 5));

// Task 14 #3 //
function half(val) {
    return val / 2;  
}

console.log(half( 99 ));

// Task 14 #4 //
function sumAndDiv( val1, val2, factor ) {
    return ( val1 + val2 ) / factor;
}

console.log(sumAndDiv(5 , 10, 3));

// Task 14 - block scoped variables //
// The variable summa used in the function sumAndDiv2
// does only exist within the function. I.e. it's scope 
// is blocked to that function. 

let summa = 98; // This varibale has a broader scope than summa within the function.
var duck = 'Kallis';

function sumAndDiv2( val1, val2, factor ) {
    let summa = val1 + val2;
    var duck = "Kalle Anka"; 
    // Even this variable "duck" has function scope since it is declared 
    // within the function despite it being defined as a "var"
    return summa / factor;
}

console.log(summa);
console.log(duck);
console.log(sumAndDiv2(50 , 50, 10));
// The use of variables with summa and duck in the function above
// has not effected the original summa and duck variables.
console.log(summa); 
console.log(duck);


// Task 14 - hoisting variables //
// JavaScript is putting the function code in memory before executing.
// I.e. assigning memory space during the context creation phase. 
// That's the reason the trainingCourse function below can be declared 
// after it´s being requested in the code below.  

trainingCourse("Front end development");

function trainingCourse(name) {
  console.log("I'm attending Scania training in " + name);
}

// Task 14 - Coersion //
// The variable type is not fixed in JavaScript as in other languages.
// Java tries to make sense of operation involving different types that
// don't match each other. 

let aString = '5'; // starting of as a string.
console.log(aString / 2 ); // Used as a number when divided

let aNumber = 6; // starting of as a number.
console.log( aString + aNumber ); // Used as a string when added to a string

let aBoolean = true; // starting of as a string.
let anotherBoolean = false;
console.log(aBoolean + ' hej' ); // Used as a string when added to a string
console.log(aBoolean + 1 ); // Used as a number when added to a number
console.log(anotherBoolean * aNumber ); // Used as a number when multiplied by a number

// Task 14 - employee function //

function Employee( eNumber, name , surname, email ) {
    this.empNumber = eNumber;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.fullName = function() {
        return this.name + " " + this.surname;
    };
    this.getContactCard = function() {
        return {
            empNr: this.empNumber,
            name:  this.fullName()
        }
    };
}

const agent7 = new Employee( 007 , 'James' , 'Bond' , 'james.bond@mi5.com' );
console.log( agent7.fullName() );
console.log( agent7.getContactCard() );

// Task 14 - project object //

const project1 = {
    nr: 1001,
    costCenter: 7305,
    description: "A JavaScript project",
    startDate: "2020-02-25",
    endDate: "2020-03-25"
}

const project2 = {
    nr: 1002,
    costCenter: 8037,
    description: "A code clean up project",
    startDate: "2020-04-25",
    endDate: "2020-08-25"
}

console.log( project1.nr + " " + project1.description );
console.log( project2.nr + " " + project2.description );